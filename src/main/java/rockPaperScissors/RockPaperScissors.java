package rockPaperScissors;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int wins = 0;
        int losses = 0;
        int round = 1;

        while(true) {
            System.out.println("Let's play round " + round + "!");
            String[] rps = {"rock", "paper", "scissors"};
            String computerMove = rps[new Random().nextInt(rps.length)];

            
            String playerMove;

            while(true) {
                System.out.println("Your choice (Rock/Paper/Scissors)? ");
                playerMove = scanner.nextLine();
                if (playerMove.equals("rock") || playerMove.equals("paper") || playerMove.equals("scissors")) {
                    break;

                }
                System.out.println("I do not understand " + playerMove + "." + " Could you try again? " );
            }
            System.out.print("Human chose " + playerMove  + ", computer chose " + computerMove + ".");

            if (playerMove.equals(computerMove)) {
                System.out.println(" It's a tie!");
            }
            else if (playerMove.equals("rock")) {
                if (computerMove.equals("paper")) {
                    System.out.println(" Computer wins!");
                    losses++;

                }   else if (computerMove.equals("scissors")) {
                    System.out.println(" Human wins!");
                    wins++;
                }
            }

            else if (playerMove.equals("paper")) {
                if (computerMove.equals("rock")) {
                    System.out.println(" Human wins!");
                    wins++;

                }   else if (computerMove.equals("scissors")) {
                    System.out.println(" Computer wins!");
                    losses++;
                }
            }

            else if (playerMove.equals("scissors")) {
                if (computerMove.equals("paper")) {
                    System.out.println(" Human wins!");
                    wins++;

                }   else if (computerMove.equals("rock")) {
                    System.out.println(" Computer wins!");
                    losses++;
                }
            }

            System.out.println("Score: human " + wins + ", computer " + losses);

            System.out.println("Do you wish to continue playing? (y/n)?");
            String playAgain = scanner.nextLine();

            if (playAgain.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

            else if (playAgain.equals("y")) {
                round++;
            }

            

    
        }
        
        

        




    }
}


